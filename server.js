var express = require('express');
var app = express();
var port = 3000;
var mongoose = require('mongoose');
var Users = require('./api/models/users');
var bodyParser = require('body-parser');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Tododb').then(()=>{
    console.log("db connected")
});


app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

var routes = require('./api/routes/users');
routes(app);
app.listen(port);

console.log('todo list RESTful API sever started on: ' + port);