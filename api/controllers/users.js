"use strict";

var mongoose = require("mongoose");
var Users = mongoose.model("Users");
var nodemailer = require("nodemailer");

exports.list_all_Users = function (req, res) {
  Users.find({}, function (err, Users) {
    if (err) res.send(err);
    console.log("Users:::", Users);
    res.json(Users);
  });
};

exports.verify_code = async function (req, res) {
  const user = await Users.findOne({
    email: {
      $eq: req.body.email,
    },
  });
  console.log("user::::::", user);
  console.log("verification body::::::", req.body);

  if (user) {
    if (user.verification_code == req.body.verification_code) {
      user.verification_code = "";
      await user.save();
      res.json("verified successfully");
    } else {
      res.json("otp not matched");
    }
  } else {
    res.json("user not found");
  }
};


exports.login = async function (req, res) {
  const user = await Users.findOne({
    email: {
      $eq: req.body.email,
    },
  });
  console.log("user::::::", user);
  console.log("verification body::::::", req.body);

  if (user) {
    if (user.password == req.body.password) {
      res.json(user);
    } else {
      res.json("password not matched");
    }
  } else {
    res.json("user not found");
  }
}
exports.create_a_Users = function (req, res) {
  var code = Math.floor(100000 + Math.random() * 900000);
  console.log("random number:::", code);
  req.body.verification_code = code;
  console.log("Users body::::", req.body);
  var new_Users = new Users(req.body);
  console.log("new_Users:::", new_Users);
  new_Users.save(function (err, user) {
    if (err) {
      console.log("error", err);
      res.send(err);
    }
    console.log("Users:::", user);

    sendMail(code, user.email);

    res.json(user);
  });
};

function sendMail(code, email) {
  console.log(email + " -- ", code);
  var transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "smritisaini5555@gmail.com",
      pass: "pacrvxmknkoepngp",
    },
  });

  var mailOptions = {
    from: "smritisaini5555@gmail.com",
    to: email,
    subject: "Sending email using Node.js",
    text: "Your Verification code is " + code,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log("mail send error", error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
}
