'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TaskSchema = new Schema({
    username: {
        type: String,
        require: 'Kindly enter the name'
    },
    first_name: {
        type: String,
        require: 'Kindly enter your first name'
    },
    last_name: {
        type: String,
        require: 'Kindly enter your last name'
    },
    password: {
        type: String,
        require: 'Kindly enter your password'
    },
    email: {
        type: String,
        require: 'Kindly enter the email'
    },
    phone_number: {
        type: Number,
        require: 'Kindly enter your phone number'
    },
    verification_code: {
        type: Number,
    },
});

 module.exports = mongoose.model('Users', TaskSchema);



