'use strict';
module.exports = function(app) {
    var users = require('../controllers/users');

    app.route('/Users')
      .get(users.list_all_Users)
      .post(users.create_a_Users)

      app.route('/Users/verification')
       .post(users.verify_code)

       app.route('/Users/login')
       .post(users.login)
     
};